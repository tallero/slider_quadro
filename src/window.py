# window.py
#
# Copyright 2020 Pellegrino Prevete
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk


@Gtk.Template(resource_path='/ml/prevete/slider_quadro/window.ui')
class SliderQuadroWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'SliderQuadroWindow'

    #label = Gtk.Template.Child()

    value = Gtk.Template.Child("value")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @Gtk.Template.Callback()
    def on_adjustment_value_changed(self, adjustment):
        value = adjustment.get_value()
        result = round(value**2 - 4, 2)
        if result > 0:
            symbol = "<span foreground='green'><b>&gt;</b></span>"
        elif result < 0:
            symbol = "<span foreground='red'><b>&lt;</b></span>"
        else:
            symbol = "<span foreground='green'><b>=</b></span>"
        self.value.set_markup("f(<span foreground='orange'><b>{}</b></span>) = <span foreground='orange'><b>{}</b></span>^2 - 4 = {} {} 0".format(value, value, result, symbol	))
        #print(adjustment.get_value())

#    @Template.Callback()
#    def on_adjustment_changed(self, *args):
#        print(*args)
#
#    @Template.Callback()
#    def on_scale_value_changed(self, *args):
#        print(*args)

#    @Template.Callback()
#    def on_scale_change_value(self, widget, *args):
#        self.value.set_text("ciao")
#        print(widget)
#        print(*args)
